# This script converts SOSUB's ASS subs to 2 SRT eng & vie
# Uses some heuristic to perform the split
# Usage:
#   python ./convertAss2Srt.py <file_name_in_input_without_.ass>
# Example:
#   python ./convertAss2Srt.py "302 - Deep ocean mysteries and wonders - David Gallo"
# To run all Input subs from Powershell:
#   Get-Item Input\* | ForEach-Object { python .\convertAss2Srt.py $_.BaseName }
import codecs
import re
import sys

VERBOSE = False

VIE_SUFFIX = " - vie"
ENG_SUFFIX = " - eng"

STARTTIME_INDEX = 1
ENDTIME_INDEX = 2
TEXT_INDEX = 9
SUB_ENTRY_SIZE = 10

SUCCESSFUL = 0
FAIL = 1

ZERO_ASS_TIME = "0:0:0.0"
def write_file(filename, mode, text):
  with codecs.open(filename, mode, 'utf-8') as fout:
    fout.write(text)

def print_n_exit(error):
  print('Error: ' + error)
  sys.exit()

# Remove formatting in sub text
def remove_formatting(text):
  return re.sub(r'{.*?}', '', text)

# returns a list of 4 int elements: [hour, minute, seconds, milliseconds]
def parse_time(t):
  parts = t.split(':')
  secs_n_10ms = parts[2].split('.')
  parts[2] = secs_n_10ms[0]
  parts.append(secs_n_10ms[1])

  parts = [int(p) for p in parts]

  # adjust to milliseconds
  parts[3] *= 10

  return parts

# Convert a time string in ass format to srt format
def convert_ass_to_srt_time(t):
  parts = parse_time(t)
  return "{!s:0>2}:{!s:0>2}:{!s:0>2},{!s:0>3}".format(*parts)

# Returns <0 if time_a < time_b
#          0 if time_a == time_b
#         >0 if time_a > time_b
def compare_time(time_a, time_b):
  time_a_parts = parse_time(time_a)
  time_b_parts = parse_time(time_b)

  for i in range(4):
    if time_a_parts[i] != time_b_parts[i]:
      return time_a_parts[i] - time_b_parts[i]

  return 0

# Separate eng and vie subs into arrays
# returns (eng_sub, vie_sub)
def extract_subs(lines):
  # dialogues lines
  dialogues = [line for line in lines if line.startswith('Dialogue')]

  # find the watermark line to break eng sub & vie sub
  found_watermark = False
  watermark_index = 0
  while watermark_index < len(dialogues):

    found_watermark = ',TEDvn.com' in dialogues[watermark_index]
    #found_watermark = dialogues[watermark_index].endswith(',TEDvn.com')
    if found_watermark:
      break

    watermark_index += 1

  if not found_watermark:
    print_n_exit("Cannot find watermark")

  # retrieve eng sub & vie sub
  eng_sub = [d.split(',', 9) for d in dialogues[:watermark_index]]
  vie_sub = [d.split(',', 9) for d in dialogues[watermark_index + 1 :]]

  if len(vie_sub) == 0 or len(eng_sub) == 0:
    print_n_exit('Cannot parse subs')

  # clean up formatting
  for s in eng_sub:
    s[TEXT_INDEX] = remove_formatting(s[TEXT_INDEX])
  for s in vie_sub:
    s[TEXT_INDEX] = remove_formatting(s[TEXT_INDEX])

  return (eng_sub, vie_sub)

# Combine sub entries from index until reaches endtime.
# Returns (status, delta, combined_text)
def find_combined_delta_n_text(sub, index, endtime):
  delta = 1
  try:
    while True:
      endtime_compare_result = compare_time(sub[index + delta][ENDTIME_INDEX], endtime)
      if endtime_compare_result == 0:
        # we got the correct delta
        break
      elif endtime_compare_result > 0:
        # can't handle different endtime
        return (FAIL, -1, '')

      delta += 1
  except IndexError:
    return (FAIL, -1, '')


  combined_text = ''
  for i in range(index, index + delta + 1):
    combined_text += sub[i][TEXT_INDEX] + ' \\N'

  # Removes the last \\N we added
  combined_text = combined_text[:-4]

  # Removes duplicated \n in case they already have it
  combined_text = combined_text.replace('\n\n', '\n')

  return (SUCCESSFUL, delta, combined_text)


# Returns (status, synced_eng_sub, synced_vie_sub)
def merge_sync(eng_sub, vie_sub):
  synced_eng_sub = []
  synced_vie_sub = []
  eng_index = 0
  vie_index = 0
  status = SUCCESSFUL

  # merge the 2 subs
  while eng_index < len(eng_sub) and vie_index < len(vie_sub):
    # we can only handle equal starttime
    if compare_time(eng_sub[eng_index][STARTTIME_INDEX], vie_sub[vie_index][STARTTIME_INDEX]) != 0:
      return (FAIL, eng_sub, vie_sub)

    endtime_compare_result = compare_time(eng_sub[eng_index][ENDTIME_INDEX], vie_sub[vie_index][ENDTIME_INDEX])

    # Simple case, both start and end time equals.
    # Take both entries and move on
    if endtime_compare_result == 0:
      synced_eng_sub.append(eng_sub[eng_index])
      synced_vie_sub.append(vie_sub[vie_index])

      eng_index += 1
      vie_index += 1
      continue

    # start time equals, but end time different.
    # Combine the entries from one of the subs.
    new_entry = [None] * SUB_ENTRY_SIZE
    if endtime_compare_result < 0:
      # eng sub needs to be combined
      (combine_status, delta, combined_text) = find_combined_delta_n_text(eng_sub, eng_index, vie_sub[vie_index][ENDTIME_INDEX])
      new_entry[STARTTIME_INDEX] = eng_sub[eng_index][STARTTIME_INDEX]
      new_entry[ENDTIME_INDEX] = eng_sub[eng_index + delta][ENDTIME_INDEX]
      new_entry[TEXT_INDEX] = combined_text

      synced_eng_sub.append(new_entry)
      eng_index += delta + 1

      synced_vie_sub.append(vie_sub[vie_index])
      vie_index += 1

    else:
      # vie sub needs to be combined
      (combine_status, delta, combined_text) = find_combined_delta_n_text(vie_sub, vie_index, eng_sub[eng_index][ENDTIME_INDEX])
      new_entry[STARTTIME_INDEX] = vie_sub[vie_index][STARTTIME_INDEX]
      new_entry[ENDTIME_INDEX] = vie_sub[vie_index + delta][ENDTIME_INDEX]
      new_entry[TEXT_INDEX] = combined_text

      synced_vie_sub.append(new_entry)
      vie_index += delta + 1

      synced_eng_sub.append(eng_sub[eng_index])
      eng_index += 1

  # assign all trailing elements
  if eng_index < len(eng_sub):
    status = FAIL
    while eng_index < len(eng_sub):
      synced_eng_sub.append(eng_sub[eng_index])
      eng_index += 1

  if vie_index < len(vie_sub):
    status = FAIL
    while vie_index < len(vie_sub):
      synced_vie_sub.append(vie_sub[vie_index])
      vie_index += 1

  return (status, synced_eng_sub, synced_vie_sub)
  
# Sync the 2 subs directly in the array
# Returns a tuple (status, synced_eng_sub, synced_vie_sub)
# status is either SUCCESSFUL or FAIL
def sync_subs(eng_sub, vie_sub):
  # If they have same # of lines.
  if (len(eng_sub) == len(vie_sub)):
    # Copy time from Eng sub to vie sub.
    for i in range(len(vie_sub)):
      vie_sub[i][STARTTIME_INDEX] = eng_sub[i][STARTTIME_INDEX]
      vie_sub[i][ENDTIME_INDEX] = eng_sub[i][ENDTIME_INDEX]
    return (SUCCESSFUL, eng_sub, vie_sub)

  # Otherwise we might need to merge some lines
  return merge_sync(eng_sub, vie_sub)

# Export to .srt files to filename.
def export_to_srt(sub, filename):
  output = ''
  for i in range(len(sub)):
    output += str(i+1) + '\r\n'
    output += convert_ass_to_srt_time(sub[i][STARTTIME_INDEX]) \
              + ' --> ' \
              + convert_ass_to_srt_time(sub[i][ENDTIME_INDEX]) \
              + '\r\n'
    output += sub[i][TEXT_INDEX] + '\r\n'
    output += '\r\n'

  write_file(filename, 'w', output)

try:
  base_filename = sys.argv[1]
except IndexError:
  # for debugging
  base_filename = '480 - Why do we have to wear sunscreen - Kevin P Boyd'

original_filename = 'Input/' + base_filename + '.ass'

print("Converting file: " + original_filename)

if VERBOSE:
  print('Reading data...')

fin = open(original_filename, 'r', encoding='utf-8')
lines = fin.read().splitlines()
fin.close()

if VERBOSE:
  print('Done!')

if VERBOSE:
  print('Extracting subs...')

(eng_sub, vie_sub) = extract_subs(lines)

if VERBOSE:
  print('Done!')

if VERBOSE:
  print('Syncing subs...')

# Heuristic: remove first vie_sub line if it's an extra
if compare_time(vie_sub[1][STARTTIME_INDEX], eng_sub[0][STARTTIME_INDEX]) == 0:
  vie_sub = vie_sub[1:]
(status, eng_sub, vie_sub) = sync_subs(eng_sub, vie_sub)


if VERBOSE:
  print('Done!')

if VERBOSE:
  print('Exporting srt files...')

output_dir = 'Output/Successful/' if status == SUCCESSFUL else 'Output/UnSuccessful/'
eng_srt_filename = output_dir + base_filename + ENG_SUFFIX + '.srt'
vie_srt_filename = output_dir + base_filename + VIE_SUFFIX + '.srt'

print('%s conversion.\n' % ('Successful' if status == SUCCESSFUL else 'UnSuccessful'))
if VERBOSE:
  print('eng_srt = ' + eng_srt_filename)
  print('vie_srt = ' + vie_srt_filename)

export_to_srt(eng_sub, eng_srt_filename)
export_to_srt(vie_sub, vie_srt_filename)

if VERBOSE:
  print('Done!')
